/******************************************************************************
 Atmel - C driver - twi driver - tests
   - myAVR MK3 board driver, implemented in C for Atmel microcontroller
 Copyright (c) 2016 - 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Two Wire Interface (TWI/I2C) driver tests.
 *
 * @file      test.c
 * @see       test.h
 * @author    Martin Singer
 * @date      2016 - 2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef F_CPU
  #define F_CPU  3686400ul // myAVR-MK2 (ATmega8)
//#define F_CPU 16000000ul // myAVR-MK3 (ATmega2560)
#endif
#ifndef F_SCL
  #define F_SCL   100000ul ///< TWI clock in Hz (F_CPU >= 16 * F_SCL, used for TWI master)
#endif


#include <avr/io.h>
#include <util/delay.h>    // requires F_CPU
#include <mars/twi/master.h>

#include "test.h"


#define DDR_LED  DDRD
#define PORT_LED PORTD


#if TEST == 1
  #error Selected TEST function is not implemented yet!


#elif TEST == 2
void test_twi_master_transmitter_reset_mode(void)
{
	uint8_t addr = 0x30; // Slave address [0|0|1|1|A2|A1|A0|R/W] --> 0111000x
	uint8_t data = 0x00;
	uint8_t ret  = 0x00;

	twi_master_init(F_CPU, F_SCL);

	while (1) {
		ret = twi_master_start(addr | TWI_MASTER_WRITE);
		if (0 != ret) {
			twi_master_stop();
		} else {
			for (char c = 'A'; c <= 'Z'; ++c) {
				data = (uint8_t) c;
				ret = twi_master_write(data);
				if (ret != 0) {
					break;
				}
			}
			twi_master_stop();
			_delay_ms(2500);
		}
	}

	return;
}


#elif TEST == 3
void test_twi_master_transmitter_address_mode(void)
{
	uint8_t addr = 0x30; // Slave address [0|0|1|1|A2|A1|A0|R/W] --> 0111000x
	uint8_t data = 0x00;
	uint8_t ret  = 0x00;

	char c = 'A';

	twi_master_init(F_CPU, F_SCL);

	/*
	 * This functions does NOT restart with 'A' if there is a twi_master_stop().
	 * It sends the byte (for which "ret" was not 0) again.
	 */
	while (1) {
		ret = twi_master_start(addr | TWI_MASTER_WRITE);
		if (0 != ret) {
			twi_master_stop();
		} else {
			ret = twi_master_write(5); // transmit the buffer address (pos)
			if (ret == 0) {
				data = (uint8_t) c;
				ret = twi_master_write(data);
				if (ret != 0) {
					break;
				}
				if (++c > 'Z') {
					c = 'A';
				}
			}
			twi_master_stop();
			_delay_ms(2500);
		}
	}

#if 0
	while (1) {
		ret = twi_master_start(addr | TWI_MASTER_WRITE);
		if (0 != ret) {
			twi_master_stop();
		} else {
			ret = twi_master_write(5); // transmit the buffer address
			if (ret == 0) {
				for (char c = 'A'; c <= 'Z'; ++c) {
					data = (uint8_t) c;
					ret = twi_master_write(data);
					if (ret != 0) {
						break;
					}
				}
			}
			twi_master_stop();
			_delay_ms(2500);
		}
	}
#endif // if 0
	return;
}


#elif TEST == 4
  #error Selected TEST function is not implemented yet!


#elif TEST == 5
  #error Selected TEST function is not implemented yet!


#elif TEST == 6
  #error Selected TEST function is not implemented yet!


#elif TEST == 7
  #error Selected TEST function is not implemented yet!


#elif TEST == 8
  #error Selected TEST function is not implemented yet!


#elif TEST == 9
  #error Selected TEST function is not implemented yet!


#elif TEST == 10
  #error Selected TEST function is not implemented yet!
//#warning Selected TEST function is not tested yet!
void test_twi_slave_receiver_port_extension(void)
{
	return;
}


#elif TEST == 11
  #error Selected TEST function is not implemented yet!


#elif TEST == 12
  #error Selected TEST function is not implemented yet!


#elif TEST == 254
/** TEST Function: TWI (Two Wire Interface) master - blinken light.
 *
 * ## IC
 * This function is tested with an PCF8574AP IC.
 * - Datasheet: <http://www.nxp.com/documents/data_sheet/PCF8574.pdf>
 * - The address byte looks like __[0|1|1|1|A2|A1|A0|R/W]__ (_Page  9_)
 * - The maximal operation frequency is 100kHz              (_Page 16_)
 *
 *
 * ## Circuit
 *
 *	                         DIP16
 *	                     ---------
 *	GND -------------A0-|01  #  16|-Vdd-- +5V  .-Rp1-- +5V
 *	GND -------------A1-|02     15|-SDA--------'------ SDA
 *	GND -------------A2-|03     14|-SCL--------.------ SCL
 *	+5V --LED>|--Rv--P0-|04  I  13|-/INT-      '-Rp2-- +5V
 *	                -P1-|05  C  12|-P7-
 *	                -P2-|06     11|-P6-
 *	                -P3-|07     10|-P5-
 *	GND ------------Vss-|08     09|-P4-
 *	                     ---------
 *	                     PCF8574AP
 *
 * ## Function
 *
 * The MC (master) initiates a Connection with the port extension PCF8674AP IC (slave).
 * When the connections works, the MC sends data bytes
 * which becomes written on the port output of the port extension IC.
 * Is a data bit low, the drain get connected to GND and the LED lights up.
 */
void example_twi_master_blink(void)
{
	uint8_t addr = 0x70; // PFC8574AP: [0|1|1|1|A2|A1|A0|R/W] --> 0111000x
	uint8_t data = 0x00;
	uint8_t ret  = 0x00;

	// Init
	twi_master_init(F_CPU, F_SCL);
	DDR_LED  |=   (1<<PD5)|(1<<PD6)|(1<<PD7);  // Output
	PORT_LED &= ~((1<<PD5)|(1<<PD6)|(1<<PD7)); // Init port LOW
//	PORT_LED |=   (1<<PD5)|(1<<PD6)|(1<<PD7);
	PORT_LED &= ~((1<<PD5) | (1<<PD6) | (1<<PD7));

	while (1) {
		_delay_ms(250);

		ret = twi_master_start(addr | TWI_MASTER_WRITE);
		if (0 != ret) {
			twi_master_stop();
			PORT_LED |=   (1<<PD7);                     // Signal Fail (001)
			PORT_LED &= ~((1<<PD5) | (1<<PD6));
		} else {
			ret = twi_master_write(data);
			twi_master_stop();
			if (0 == ret) {
				PORT_LED |=   (1<<PD5);             // Signal OK (100)
				PORT_LED &= ~((1<<PD6) | (1<<PD7));
			} else {
				PORT_LED |=   (1<<PD6);             // Signal Fail (010)
				PORT_LED &= ~((1<<PD5) | (1<<PD7));
			}
		}

//		if (0x00 == data) {
		if (0xF0 == data) {
//			data = 0xFF; // LEDs off
			data = 0x0F;
		} else {
//			data = 0x00; // LEDs on
			data = 0xF0;
		}
	}

	return;
}


#elif TEST == 255
/** TEST Function: TWI (Two Wire Interface) master - button and light.
 *
 * ## IC
 * This function is tested with an PCF8574AP IC.
 * - Datasheet: <http://www.nxp.com/documents/data_sheet/PCF8574.pdf>
 * - The address byte looks like __[0|1|1|1|A2|A1|A0|R/W]__ (_Page  9_)
 * - The maximal operation frequency is 100kHz              (_Page 16_)
 * - The I/Os should be HIGH before being used as inputs    (_Page 12_)
 * - This IC has a 100uA current driver.
 *   Because of this, the button SW0 requires no pre resistor or pull-up resistor.
 *   <https://www.mikrocontroller.net/articles/Port-Expander_PCF8574>
 *
 *
 * ## Circuit
 *
 *	                         DIP16
 *	                     ---------
 *	GND -------------A0-|01  #  16|-Vdd-- +5V  .-Rp1-- +5V
 *	GND -------------A1-|02     15|-SDA--------'------ SDA
 *	GND -------------A2-|03     14|-SCL--------.------ SCL
 *	+5V --LED>|--Rv--P0-|04  I  13|-/INT-      '-Rp2-- +5V
 *	                -P1-|05  C  12|-P7-------SW0------ GND
 *	                -P2-|06     11|-P6-
 *	                -P3-|07     10|-P5-
 *	GND ------------Vss-|08     09|-P4-
 *	                     ---------
 *	                     PCF8574AP
 *
 * ## Function
 *
 * The MC (master) initiates a Connection with the port extension PCF8674AP IC (slave).
 * When the connections works, the MC sends 0xFF for init the PORTs first.
 * The I/Os should be HIGH before being used as inputs.
 * The TWI-IC becomes read, the state of P7 (button SW0) checked and P0 (LED)
 * depending on the state of P7 written.
 */
void example_twi_master_button(void)
{
	uint8_t addr = 0x70; // PFC8574AP: [0|1|1|1|A2|A1|A0|R/W] --> 0111000x
	uint8_t ret  = 0x00;
	uint8_t data = 0x00;

	// Init
	twi_master_init(F_CPU, F_SCL);
	DDR_LED  |=   (1<<PD5)|(1<<PD6)|(1<<PD7);  // Output
	PORT_LED &= ~((1<<PD5)|(1<<PD6)|(1<<PD7)); // Init port LOW
	PORT_LED |=   (1<<PD5)|(1<<PD6)|(1<<PD7);

	// Init TWI IC PORTs
	ret = twi_master_start(addr | TWI_MASTER_WRITE);
	if (0 != ret) {
		twi_master_stop();
		PORT_LED |=   (1<<PD7);                     // Signal Fail (001)
		PORT_LED &= ~((1<<PD5) | (1<<PD6));
	} else {
		ret = twi_master_write(0xFF); // set all PORTs HIGH (to VDD)
		twi_master_stop();
		if (0 == ret) {
			PORT_LED |=   (1<<PD5);             // Signal OK (100)
			PORT_LED &= ~((1<<PD6) | (1<<PD7));
		} else {
			PORT_LED |=   (1<<PD6);             // Signal Fail (010)
			PORT_LED &= ~((1<<PD5) | (1<<PD7));
		}
	}

	// Test Loop
	while (1) {
		// read button
		ret = twi_master_start(addr | TWI_MASTER_READ);
		if (0 != ret) {
			twi_master_stop();
			PORT_LED |=   (1<<PD7);                     // Signal Fail (001)
			PORT_LED &= ~((1<<PD5) | (1<<PD6));
		} else {
			data = twi_master_read_nak();         // read and stop twi
			PORT_LED |=  ((1<<PD5) | (1<<PD7));         // Signal NAK (101)
			PORT_LED &= ~((1<<PD6));
		}
		// set led
		ret = twi_master_start(addr | TWI_MASTER_WRITE);
		if (0 != ret) {
			twi_master_stop();
			PORT_LED |=   (1<<PD7);                     // Signal Fail (001)
			PORT_LED &= ~((1<<PD5) | (1<<PD6));
		} else {
			if (data & 0x80) {                    // if  PIN7 is HIGH (Button released)
				ret = twi_master_write(0xFE); // set PIN0 to LOW  (LED on)
			} else {
				ret = twi_master_write(0xFF); // set PIN0 to HIHG (LED off)
			}
			twi_master_stop();
			if (0 == ret) {
				PORT_LED |=   (1<<PD5);             // Signal OK (100)
				PORT_LED &= ~((1<<PD6) | (1<<PD7));
			} else {
				PORT_LED |=   (1<<PD6);             // Signal Fail (010)
				PORT_LED &= ~((1<<PD5) | (1<<PD7));
			}
		}
	}
	return;
}
#endif // TEST

