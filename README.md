Atmel - C drivers
=================

  driver development in C for Atmel microcontroller,
    using the MyAVR MK3 development board

Copyright (c) 2017 Martin Singer <martin.singer@web.de>


About
=====

Testing the Two Wire Interface (TWI/I2C) driver.

This file is part of myAVR-MK3_c-test_libtwi.


Requirements
------------

### General

* avr-gcc
* avrdude


### Libraries

Depends on:

* `/usr/local/avr/include/twi/master.h`
* `/usr/local/avr/lib/avr4/libtwi.a`

See <https://gitlab.com/marsin/myAVR-MK3_c-driver_libtwi>


Documentation
-------------

Creating the Doxygen reference:

	$ doxygen doxygen.conf
	$ firefox doc/html/index.html


Usage
-----

Read and edit the file `src/Makefile.am`.

* `make all`        Make software.
* `make clean`      Clean out built project files.
* `make program`    Download the hex file to the device, using avrdude.
                    Please customize the avrdude settings below first!
* `make install`    Install drivers as library and headers on system.
* `make uninstall`  Uninstall driver library and headers from system.

To rebuild project do `make clean` then `make all`.

